import React from "react";
import Link, { Props } from "../atoms/LinkComponent";

const ButtonLink: React.FC<Props> = ({ children, href, className }) => {
  return (
    <Link
      href={href}
      className={`inline-flex items-center border border-transparent rounded-full shadow-sm max-w-xs ${
        className || ""
      } `}>
      {children}
    </Link>
  );
};

export default ButtonLink;
