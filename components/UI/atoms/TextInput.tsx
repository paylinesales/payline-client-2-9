import { ReactElement } from "react";

/* eslint-disable @typescript-eslint/no-explicit-any */
interface ITextInput {
  htmlFor: string;
  labelClassName?: string;
  labelText?: string;
  inputId: string;
  inputName: string;
  inputType: string;
  autoComplete?: string;
  required: boolean;
  inputClassName?: string;
  inputContainerClassName?: string;
  field?: any;
  meta?: any;
}

const TextInput = ({
  htmlFor,
  labelClassName = "block text-sm font-medium text-gray-700",
  labelText,
  inputId,
  inputName,
  inputType,
  autoComplete = "",
  required,
  inputClassName = "focus:ring-light-blue-500 focus:border-light-blue-500 flex-grow block w-full min-w-0 rounded sm:text-sm border-gray-300 border border-solid border-payline-black",
  inputContainerClassName = "",
  field,
  meta,
}: ITextInput): ReactElement => (
  <div className={inputContainerClassName}>
    <label htmlFor={htmlFor} className={labelClassName}>
      {labelText}
    </label>
    <div>
      <input
        id={inputId}
        name={inputName}
        type={inputType}
        autoComplete={autoComplete}
        required={required}
        className={inputClassName}
        {...field}
      />
      {meta?.touched && meta?.error && (
        <div className="text-xs">{meta?.error}</div>
      )}
    </div>
  </div>
);

TextInput.defaultProps = {
  labelClassName: "block text-sm font-medium text-gray-700",
  inputClassName:
    "focus:ring-light-blue-500 focus:border-light-blue-500 flex-grow block w-full min-w-0 rounded sm:text-sm border-gray-300 border border-solid border-payline-black",
  labelText: "",
  autoComplete: "",
  inputContainerClassName: "",
  field: {},
  meta: {},
};

export default TextInput;
