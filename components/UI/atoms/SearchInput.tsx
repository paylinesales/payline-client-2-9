/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable  react/require-default-props, react/no-unused-prop-types, react/forbid-prop-types */
import Image from "next/image";
import { ApolloError, useApolloClient } from "@apollo/client";
import React, { ReactElement, useState, useEffect } from "react";
import { useRouter } from "next/router";
import blogSearchQueries from "../../../queries/blog-search";

interface IQuerySetterProps {
  setLoading: React.Dispatch<React.SetStateAction<boolean>> | undefined;
  setError:
    | React.Dispatch<React.SetStateAction<ApolloError | undefined>>
    | undefined;
  setData: React.Dispatch<React.SetStateAction<any>> | undefined;
}

interface ISearchInputProps {
  categories?: Array<string>;
  search?: string;
  setSearch?: React.Dispatch<React.SetStateAction<string>>;
  querySetters?: IQuerySetterProps;
}

const SearchInput: React.FC<ISearchInputProps> = (props): ReactElement => {
  const {
    categories = [""],
    search = "",
    setSearch,
    querySetters = {
      setLoading: undefined,
      setError: undefined,
      setData: undefined,
    },
  } = props;
  const [initialLoad, setInitial] = useState(false);
  if (search) {
    /**
     * Runs on component mount to handle searching if redirected from somewhere else
     */
    useEffect(() => {
      setInitial(true);
    }, []);
  }
  const { setLoading, setError, setData } = querySetters;
  const client = useApolloClient();
  if (setLoading && setError && setData && setSearch) {
    const searchBlog = async (event) => {
      setLoading(true);
      event.preventDefault();
      const { error, data } = await client.query({
        query: blogSearchQueries,
        variables: { categories, search },
      });
      setLoading(false);
      setError(error);
      setData(data);
      // in order to write this query, SearchInput needs to know which button in CategoriesPicker is highlighted
    };
    if (initialLoad) {
      searchBlog(new Event("firstSearch"));
      setInitial(false);
    }
    const handleInputChange = (event) => {
      setSearch(event.target.value);
    };
    return (
      <div className="flex justify-between bg-white py-1 px-3 h-50 w-96 rounded-lg flex-wrap">
        <form onSubmit={searchBlog} className="flex flex-grow gap-3">
          <Image
            src="/images/svg/magnifying-glass.svg"
            width={18}
            height={22}
          />
          <input
            placeholder="Search"
            value={search}
            className="flex-1"
            type="text"
            onChange={handleInputChange}
          />
          {/* eslint-disable-next-line react/button-has-type */}
          <button className="flex items-center justify-center">
            <Image
              src="/images/svg/arrow-right-black.svg"
              width={18}
              height={22}
            />
          </button>
        </form>
      </div>
    );
  }
  /** form on submit pass search to blog-search */
  const router = useRouter();
  const handleBlog = (e) => {
    e.preventDefault();
    const newSearch = e.target[0].value;
    router.push({ pathname: "/blog-search", query: `search=${newSearch}` });
  };
  return (
    <div className="flex justify-between bg-white py-1 px-3 h-50 w-96 rounded-lg flex-wrap">
      <form onSubmit={handleBlog} className="flex flex-grow gap-3">
        <Image src="/images/svg/magnifying-glass.svg" width={18} height={22} />
        <input placeholder="Search" className="flex-1" type="text" />
        {/* eslint-disable-next-line react/button-has-type */}
        <button className="flex items-center justify-center">
          <Image
            src="/images/svg/arrow-right-black.svg"
            width={18}
            height={22}
          />
        </button>
      </form>
    </div>
  );
};

export default SearchInput;
