/* eslint-disable  react/require-default-props, react/no-unused-prop-types, react/forbid-prop-types */
import React from "react";

interface IWhiteButtonProps {
  className?: string;
  children?: React.ReactNode;
  onClick?: (event: React.MouseEvent<HTMLButtonElement>) => void;
  disabled?: boolean;
  value?: number | string;
  selected?: boolean;
}

const WhiteButton: React.FC<IWhiteButtonProps> = (props) => {
  const {
    children,
    className = "",
    onClick,
    disabled = false,
    value,
    selected = false,
  } = props;
  const cls = `border border-solid border-payline-border-light rounded p-3 ${className} ${
    selected ? "bg-payline-dark text-white" : "text-payline-dark bg-white"
  }`;
  return (
    <button
      disabled={disabled}
      type="button"
      onClick={onClick}
      className={cls}
      data-value={value}>
      {children}
    </button>
  );
};

WhiteButton.defaultProps = {
  className: "",
  children: null,
  onClick() {
    // eslint-disable-next-line no-console
    console.log("Clicked");
  },
  disabled: false,
  value: "",
};

export default WhiteButton;
