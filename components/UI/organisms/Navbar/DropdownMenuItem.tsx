import React from "react";
import Image from "next/image";
import Link from "../../atoms/LinkComponent";

type dropdownMenuItemProps = {
  imageSrc: string;
  imageSize?: number;
  title: string;
  href: string;
  className?: string;
  altText: string;
};

const DropdownMenuItem: React.FC<dropdownMenuItemProps> = ({
  imageSrc,
  imageSize = 56,
  title,
  href: link,
  altText,
  className: classes = "",
}) => {
  return (
    <Link href={link}>
      <div
        className={`flex items-center p-2 border-2  hover:border-payline-border-light transition-colors duration-300 ${classes}`}>
        <Image
          src={imageSrc}
          width={imageSize}
          height={imageSize}
          alt={altText}
        />
        <span className="ml-6">{title}</span>
      </div>
    </Link>
  );
};

DropdownMenuItem.defaultProps = {
  imageSize: 56,
  className: "",
};

export default DropdownMenuItem;
