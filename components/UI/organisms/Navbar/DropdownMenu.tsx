/* eslint-disable @typescript-eslint/no-explicit-any */
import React from "react";
import { useBusinessCategory } from "context/BusinessCategoryContext";
import { PAYMENT_TYPES } from "../../../../constants";
import DropdownMenuItem from "./DropdownMenuItem";

const { ONLINE } = PAYMENT_TYPES;

type dropdownMenuProps = {
  onMouseLeave: (event: React.MouseEvent<HTMLDivElement>) => void;
  items: any;
};

const DropdownMenu: React.FC<dropdownMenuProps> = ({ onMouseLeave, items }) => {
  const { paymentType, businessType, slug } = useBusinessCategory();
  const { navChild } = items;
  const isOnlineBusiness = paymentType === ONLINE;
  // eslint-disable-next-line no-console
  console.log(`Current Solutions:`, items);
  const onlineLinkURL = () => {
    if (slug === undefined) return "small-business/online";
    return `${businessType}/online`;
  };
  const inPersonLinkURL = () => {
    if (slug === undefined) return "small-business/in-person";
    return `${businessType}/in-person`;
  };
  return (
    <div className="dropdown-menu bg-payline-white" onMouseLeave={onMouseLeave}>
      <div className="container mx-auto px-8 flex">
        <div className="w-9/12 py-10 pr-10">
          <h4 className="font-semibold text-payline-dark mb-6">Products</h4>
          <div className="grid grid-cols-4 gap-x-4 gap-y-6">
            {navChild.map((child) => {
              return (
                <DropdownMenuItem
                  imageSrc={child.icon.sourceUrl}
                  title={child.linkName}
                  altText={child.icon.altText}
                  href={child.link}
                  className="border-payline-white hover:bg-payline-background-light text-payline-black"
                />
              );
            })}
          </div>
        </div>
        <div className="w-3/12 py-10 px-6 bg-payline-background-light rounded-l-xl">
          <h4 className="font-semibold text-payline-dark mb-6">Payment Type</h4>
          <DropdownMenuItem
            imageSrc="/images/svg/online.svg"
            altText="Online"
            imageSize={40}
            title="Online"
            href={onlineLinkURL()}
            className={`mb-6 font-bold text-2xl py-4 border-payline-background-light hover:bg-payline-white ${
              isOnlineBusiness || slug === undefined
                ? "text-payline-black"
                : "text-payline-disabled"
            }`}
          />
          <DropdownMenuItem
            imageSrc="/images/svg/in-person.svg"
            altText="In Person"
            imageSize={40}
            title="In Person"
            href={inPersonLinkURL()}
            className={`font-bold text-2xl py-4 border-payline-background-light hover:bg-payline-white ${
              !isOnlineBusiness || slug === undefined
                ? "text-payline-black"
                : "text-payline-disabled"
            }`}
          />
        </div>
      </div>
    </div>
  );
};

export default DropdownMenu;
