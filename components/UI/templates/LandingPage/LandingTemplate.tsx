import React from "react";
import Head from "next/head";
import LogoBar from "components/UI/organisms/banners/LogoBar";
import FAQSection from "components/UI/templates/sections/home/FAQSection";
import MainHero from "components/UI/templates/sections/home/mainHeroSection";
import CertifiedIndustryLeaderSection from "components/UI/templates/sections/home/CertifiedIndustryLeaderSection/CertifiedIndustryLeaderSection";
import BlogSection from "components/UI/templates/sections/home/BlogSection/BlogSection";
import LetsGetInTouch from "components/UI/templates/sections/home/LetsGetInTouch/LetsGetInTouchSection";
import SolutionsTailoredToBusiness from "components/UI/templates/sections/home/SolutionsTailoredToBusinessSection";
import CalculateMonthlyCost from "components/UI/templates/sections/home/CalculateYourMonthlyCost/CalculateYourMonthlyCostSection";
import SolutionForYourNeeds from "components/UI/templates/sections/home/SolutionForYourNeeds";
import RevenueSection from "components/UI/templates/sections/home/RevenueSection";
import { WebsiteCopy } from "@/generated/apolloComponents";

// type landingPageLayoutProps = {
//   slug: string | Array<string> | undefined;
// };

interface LandingPageProps {
  websiteCopyData: WebsiteCopy;
  landingPageData: string;
}

const LandingPageLayout: React.FC<LandingPageProps> = ({
  websiteCopyData,
  landingPageData,
}) => {
  const { calculatorSection, faqSection } = websiteCopyData;

  console.log(landingPageData);

  const parsedData = JSON.parse(landingPageData);
  // https://wesbos.com/destructuring-renaming
  const { pages: destructuredPagedData, websiteCopy } = parsedData.data;
  const { logoBar } = websiteCopy;
  const { heroSection, title, revenueSection /** getInTouchSection * */ } =
    destructuredPagedData.edges[0].node;
  return (
    <>
      <Head>
        <title>{title}</title>
      </Head>
      {/* dynamic section: MainHero */}
      <MainHero data={heroSection} />
      <LogoBar logoBar={logoBar} />
      {/* dynamic section: Revenue section */}
      <RevenueSection revenueSection={revenueSection} />
      {/* Todo: Add Data Types */}
      <CalculateMonthlyCost calculatorData={calculatorSection} />
      {/* Todo: Add Data Types */}
      <SolutionsTailoredToBusiness />
      {/* Todo: Add Data Types */}
      {/* dynamic section: solution that suits need */}
      <SolutionForYourNeeds />

      {/* Todo: Add Data Types */}
      <LetsGetInTouch />
      {/* Todo: Verify Data Types */}
      <FAQSection faqData={faqSection} />
      {/* Todo: Add Data Types */}
      <BlogSection />
      {/* Todo: Add Data Types */}
      <CertifiedIndustryLeaderSection />
    </>
  );
};

export default LandingPageLayout;
