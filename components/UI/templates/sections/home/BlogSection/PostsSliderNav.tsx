/* eslint-disable @typescript-eslint/no-explicit-any */
import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faAngleLeft, faAngleRight } from "@fortawesome/free-solid-svg-icons";

const PostsSliderNav: React.FC<{
  navPrevRef: any;
  navNextRef: any;
}> = (props) => {
  const { navPrevRef, navNextRef } = props;
  return (
    <>
      <div className="container mx-auto flex justify-around mt-8">
        <a
          href="/blog"
          className="font-bold text-payline-black text-2xl hover:underline flex gap-8 items-center pl-3">
          See All
          <FontAwesomeIcon icon={faAngleRight} />
        </a>
        <nav className="flex gap-9 font-bold text-payline-black text-3xl">
          <div
            ref={navPrevRef}
            className="blog-section navigation-button cursor-pointer">
            <FontAwesomeIcon icon={faAngleLeft} />
          </div>
          <div
            ref={navNextRef}
            className="blog-section navigation-button cursor-pointer">
            <FontAwesomeIcon icon={faAngleRight} />
          </div>
        </nav>
      </div>
    </>
  );
};

export default PostsSliderNav;
