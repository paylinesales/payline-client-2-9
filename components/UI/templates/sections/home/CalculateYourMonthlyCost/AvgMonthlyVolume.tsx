import React from "react";
import classes from "./customInput.module.css";

const AvgMonthlyVolume: React.FC<{
  onChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
}> = (props) => {
  const { onChange } = props;
  const inputClasses = `text-payline-black py-2 ${classes.numberInput}`;
  return (
    <>
      <div className="flex flex-wrap mb-4 gap-4 items-center">
        <p className="text-md font-hkgrotesk text-payline-black uppercase lg:normal-case">
          Average monthly volume:
        </p>
        <div className="border border-solid border-payline-border-light flex justify-between bg-white py-1 px-3 h-50 rounded-lg flex-grow">
          <div className="flex items-center gap-3">
            <div className="font-bold">$</div>
            <input
              className={inputClasses}
              type="number"
              min="0"
              step="0.01"
              onChange={onChange}
              placeholder="10.000"
            />
          </div>
        </div>
      </div>
    </>
  );
};

export default AvgMonthlyVolume;
