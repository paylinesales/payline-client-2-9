import React from "react";
import { useBusinessCategory } from "context/BusinessCategoryContext";
import SectionTitle from "@/components/UI/atoms/SectionTitle";
import { WebsiteCopy_Calculatorsection as CalculatorSectionType } from "@/generated/apolloComponents";
import Calculator from "./Calculator";
import Plans from "./Plans";
import { PAYMENT_TYPES } from "../../../../../../constants";

const { ONLINE } = PAYMENT_TYPES;
interface Props {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  calculatorData: CalculatorSectionType | any;
  className?: string;
}

const CalculateYourMonthlyCostSection = ({
  className: classes,
  calculatorData,
}: Props): React.ReactElement => {
  const { paymentType, slug } = useBusinessCategory();
  // Print Calculator section to console
  const {
    sectionTitle: { highlight, subtitle, title },
    onlineData,
    inPersonData,
  } = calculatorData!;

  // for as long as the payment type is on line then the business is online
  const isOnlineBusiness = paymentType === ONLINE;

  // Determine section partial classes based on the current business type query parameter
  const getBackgroundClass = () => {
    let backgroundClass;
    if (slug === undefined) {
      return "bg-white";
    }
    if (isOnlineBusiness) {
      backgroundClass = "bg-calculate-green";
    } else {
      backgroundClass = "bg-calculate";
    }
    return backgroundClass;
  };
  const sectionClasses = `bg-cover bg-no-repeat py-24 px-2 md:px-18 lg:px-20 xl:px-32 ${getBackgroundClass()} ${classes}`;

  return (
    <section id="calculate-monthly-cost-section" className={sectionClasses}>
      <div className="container mx-auto">
        <SectionTitle
          highlighedText={highlight}
          subtitle={subtitle}
          lastPartOfHeadline={title}
        />
        <div className="lg:w-full xl:w-10/12 mx-auto">
          <Calculator />
          <Plans inPersonData={inPersonData} onlineData={onlineData} />
        </div>
      </div>
    </section>
  );
};

CalculateYourMonthlyCostSection.defaultProps = {
  className: "",
};

export default CalculateYourMonthlyCostSection;
