module.exports = {
  reactStrictMode: true,
  images: {
    domains: ["images.unsplash.com", "paylinelocal.wpengine.com"],
  },
  async redirects() {
    return [
      {
        source: "/visa-interchange-rates",
        destination: "/pricing",
        permanent: false,
      },
      {
        source: "/credit-card-processing-pricing",
        destination: "/pricing",
        permanent: false,
      },
      {
        source: "/american-express-interchange-rates",
        destination: "/pricing#american-express",
        permanent: false,
      },
      {
        source: "/mastercard-interchange-rates",
        destination: "/pricing",
        permanent: false,
      },
      {
        source: "/discover-interchange-rates#discover-card",
        destination: "/pricing",
        permanent: false,
      },
      {
        source: "/",
        destination: "/small-business/online",
        permanent: true,
      },
      {
        source: "/online-payment-processing-1",
        destination: "/small-business/online",
        permanent: true,
      },
      {
        source: "/mobile-payments-processing",
        destination: "/small-business/online",
        permanent: true,
      },
      {
        source: "/retail-credit-card-processing-1-2",
        destination: "/small-business/online",
        permanent: true,
      },
    ];
  },
};
