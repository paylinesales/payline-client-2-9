import { gql } from "@apollo/client";

const grabPostQuery = gql`
  query grabPostQuery($id: ID!) {
    post(id: $id) {
      slug
      date
      title
      featuredImage {
        node {
          sourceUrl
        }
      }
      excerpt
      author {
        node {
          avatar {
            url
          }
          url
          firstName
          lastName
        }
      }
      categories(first: 10000) {
        nodes {
          id
          name
        }
      }
    }
  }
`;

export default grabPostQuery;
